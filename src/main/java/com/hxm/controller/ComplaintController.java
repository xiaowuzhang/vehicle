package com.hxm.controller;

import com.hxm.model.SysComplaint;
import com.hxm.service.ComplaintService;
import com.hxm.util.PageTableRequest;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @date 2020/12/4 1:23
 */
@Controller
@RequestMapping("/complaint")
public class ComplaintController {
    @Autowired
    private ComplaintService complaintService;

    @GetMapping("/complaintList")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:complaint:query')")
    public Result getFixList(PageTableRequest pageTableRequest){
        pageTableRequest.countOffset();
        return complaintService.getList(pageTableRequest.getOffset(),pageTableRequest.getLimit());
    }


    @GetMapping("/add")
    @PreAuthorize("hasAuthority('sys:complaint:add')")
    public String addPage(Model model){
        model.addAttribute("sysComplaint",new SysComplaint());
        return "complaint/complaint-add";
    }

    @PostMapping("/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:complaint:add')")
    public Result add(SysComplaint sysComplaint){

        return complaintService.add(sysComplaint);
    }

    @PostMapping("/handle")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:complaint:handle')")
    public Result handle(SysComplaint sysComplaint){

        return complaintService.handle(sysComplaint);
    }

}
