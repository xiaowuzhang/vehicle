package com.hxm.controller;

import com.hxm.dto.RoleDto;
import com.hxm.model.SysRole;
import com.hxm.service.RoleService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @date 2020/12/3 17:22
 */
@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    RoleService roleService;


    @GetMapping("/all")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('sys:role:query')")
    public Result getAllRole(){
        return roleService.getAllRole();
    }


    @PostMapping("/delete")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('sys:role:del')")
    public Result deleteRoleById(Integer id){
        return roleService.deleteRoleById(id);
    }

    @GetMapping("/search")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('sys:role:query')")
    public Result search(String rolename){
        return roleService.search(rolename);
    }

    @GetMapping("/add")
    @PreAuthorize("hasAnyAuthority('sys:role:add')")
    public String addRolePage(Model model){
        model.addAttribute("sysRole",new SysRole());
        return "role/role-add";
    }

    @PostMapping("/add")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('sys:role:add')")
    public Result addRole(@RequestBody RoleDto roleDto){
        return roleService.addRole(roleDto);
    }

    @GetMapping("/edit")
    @PreAuthorize("hasAnyAuthority('sys:role:edit')")
    public String editRolePage(Integer id,Model model){
        SysRole sysRole = roleService.getRole(id);
        model.addAttribute("sysRole",sysRole);
        return "role/role-edit";
    }

    @PostMapping("/edit")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('sys:role:edit')")
    public Result editRole(@RequestBody RoleDto roleDto){
        return roleService.editRole(roleDto);
    }

}