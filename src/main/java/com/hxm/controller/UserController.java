package com.hxm.controller;

import com.hxm.config.UserId;
import com.hxm.model.SysUser;
import com.hxm.service.UserService;
import com.hxm.util.PageTableRequest;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @date 2020/12/3 15:50
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/userList")
    @ResponseBody
    //@PreAuthorize("hasAuthority('sys:user:query')")
    public Result getUserList(PageTableRequest pageTableRequest){
        pageTableRequest.countOffset();
        return userService.getUserList(pageTableRequest.getOffset(),pageTableRequest.getLimit());
    }

    @GetMapping("/add")
    @PreAuthorize("hasAuthority('sys:user:add')")
    public String addUserPage(Model model){
        model.addAttribute("sysUser",new SysUser());
        return "user/user-add";
    }

    @PostMapping("/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:user:add')")
    public Result addUser(SysUser sysUser,Integer roleId,Integer departmentId){
        sysUser.setStatus(1);
        return userService.addUser(sysUser,roleId,departmentId);
    }

    @GetMapping("/edit")
    @PreAuthorize("hasAuthority('sys:user:edit')")
    public String editUserPage(Integer id,Model model){
        SysUser sysUser  = userService.getUserById(id);
        model.addAttribute("sysUser",sysUser);
        return "user/user-edit";
    }

    @PostMapping("/edit")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:user:edit')")
    public Result editUser(SysUser sysUser,Integer roleId,Integer departmentId){
        return userService.updateUser(sysUser,roleId,departmentId);
    }



    @PostMapping("/changeStatus")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:user:lock')")
    public Result changeStatus(SysUser sysUser){
        return userService.changeStatus(sysUser);
    }


    @PostMapping("/delete")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:user:del')")
    public Result deleteUser(SysUser sysUser){
        return userService.deleteUser(sysUser);
    }


    @GetMapping("/search")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:user:query')")
    public Result getUserByFuzzyKeywords(PageTableRequest pageTableRequest, String nickname, String userId, String phone, String cellphone,String department){
        pageTableRequest.countOffset();

        return userService.getUserByFuzzyKeywords(pageTableRequest.getOffset(),pageTableRequest.getLimit(),nickname,userId,phone,cellphone,department);
    }


    @PostMapping("/deleteAll")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:user:del')")
    public Result deleteAllUser(String data){
        List<SysUser> list= com.alibaba.fastjson.JSON.parseArray(data, SysUser.class);
        return userService.deleteAllUser(list);
    }

    @GetMapping("/changePassword")
    public String changePasswordPage(Model model,@UserId Integer id){
        model.addAttribute("sysUser",userService.getUserById(id));
        return "/user/user-password";
    }

    @PostMapping("/changePassword")
    @ResponseBody
    public Result changePassword(@UserId Integer id, SysUser sysUser,String oldpassword){
        sysUser.setId(id);
        return userService.changePassword(sysUser,oldpassword);
    }



}

