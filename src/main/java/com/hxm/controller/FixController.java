package com.hxm.controller;


import com.hxm.model.SysFix;
import com.hxm.model.SysUser;
import com.hxm.service.FixService;
import com.hxm.util.PageTableRequest;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @date 2020/12/3 19:02
 */
@Controller
@RequestMapping("/fix")
public class FixController {
    @Autowired
    private FixService fixService;



    @GetMapping("/fixList")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:fix:query')")
    public Result getFixList(PageTableRequest pageTableRequest){
        pageTableRequest.countOffset();
        return fixService.getFixList(pageTableRequest.getOffset(),pageTableRequest.getLimit());
    }


    @GetMapping("/add")
    @PreAuthorize("hasAuthority('sys:user:add')")
    public String addFixPage(Model model){
        model.addAttribute("sysFix",new SysFix());
        return "fix/fix-add";
    }

    @PostMapping("/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:fix:add')")
    public Result add(SysFix sysFix){

        return fixService.add(sysFix);
    }

    @GetMapping("/edit")
    @PreAuthorize("hasAuthority('sys:fix:edit')")
    public String editFixPage(Integer id,Model model){
        SysFix sysFix  = fixService.getById(id);
        model.addAttribute("sysFix",sysFix);
        return "fix/fix-edit";
    }

    @PostMapping("/edit")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:fix:edit')")
    public Result edit(SysFix sysFix){
        return fixService.update(sysFix);
    }



    @PostMapping("/delete")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:fix:del')")
    public Result delete(Integer id){
        return fixService.delete(id);
    }


    @GetMapping("/search")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:fix:query')")
    public Result search(PageTableRequest pageTableRequest, String name){
        pageTableRequest.countOffset();

        return fixService.search(pageTableRequest.getOffset(),pageTableRequest.getLimit(),name);
    }
}
