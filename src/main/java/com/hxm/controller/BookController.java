package com.hxm.controller;

import com.hxm.model.SysBook;
import com.hxm.service.BookService;
import com.hxm.util.PageTableRequest;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * @date 2020/12/4 17:18
 */
@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/list")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:book:query')")
    public Result getList(PageTableRequest pageTableRequest){
        pageTableRequest.countOffset();
        return bookService.getList(pageTableRequest.getOffset(),pageTableRequest.getLimit());
    }


    @GetMapping("/add")
    @PreAuthorize("hasAuthority('sys:book:add')")
    public String addPage(Model model){
        model.addAttribute("sysBook",new SysBook());
        return "book/book-add";
    }

    @PostMapping("/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:book:add')")
    public Result add(SysBook sysBook, HttpServletRequest req) throws UnsupportedEncodingException {
        req.setCharacterEncoding("utf-8");
        return bookService.add(sysBook);
    }

    @GetMapping("/edit")
    @PreAuthorize("hasAuthority('sys:book:edit')")
    public String editPage(Integer id,Model model){
        SysBook sysBook  = bookService.getById(id);
        model.addAttribute("sysBook",sysBook);
        return "book/book-edit";
    }

    @PostMapping("/edit")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:book:edit')")
    public Result edit(SysBook sysBook) {

        return bookService.edit(sysBook);
    }

    @PostMapping("/del")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:book:edit')")
    public Result del(SysBook sysBook){

        return bookService.del(sysBook);
    }

    @GetMapping("/search")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:book:query')")
    public Result search(String carNumber, PageTableRequest pageTableRequest){
        pageTableRequest.countOffset();
        Result result = bookService.search(carNumber, pageTableRequest.getOffset(), pageTableRequest.getLimit());
        return result;
    }
}
