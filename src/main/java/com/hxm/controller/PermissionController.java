package com.hxm.controller;


import com.alibaba.fastjson.JSONArray;
import com.hxm.model.SysPermission;
import com.hxm.service.PermissionService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @date 2020/12/3 15:34
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    PermissionService permissionService;



    @GetMapping("/listAllPermission")
    @ResponseBody
    public Result listAllPermission(){
        return permissionService.listAllPermission();
    }

    @GetMapping("/listAllPermissionByRoleId")
    @ResponseBody
    public Result listAllPermissionByRoleId(Integer id){
        return permissionService.listAllPermissionByRoleId(id);
    }

    @GetMapping("/menu")
    @ResponseBody
    public  Result getMenu(Integer userId){
        return permissionService.getMenu(userId);
    }



}
