package com.hxm.controller;


import com.hxm.service.RoleUserService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/roleuser")
public class RoleUserController {

    @Autowired
    RoleUserService roleUserService;

    @PostMapping("/getRoleUserByUserId")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('sys:role:query')")
    public Result getRoleUserByUserId(Integer userId){

        return roleUserService.getRoleUserByUserId(userId);
    }
}
