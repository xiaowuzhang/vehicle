package com.hxm.controller;

import com.hxm.model.SysCar;
import com.hxm.service.CarService;
import com.hxm.util.PageTableRequest;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @date 2020/12/4 2:26
 */
@Controller
@RequestMapping("/car")
public class CarController {
    @Autowired
    private CarService carService;

    @GetMapping("/list")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:car:query')")
    public Result getFixList(PageTableRequest pageTableRequest){
        pageTableRequest.countOffset();
        return carService.getList(pageTableRequest.getOffset(),pageTableRequest.getLimit());
    }


    @GetMapping("/add")
    @PreAuthorize("hasAuthority('sys:car:add')")
    public String addFixPage(Model model){
        model.addAttribute("sysCar",new SysCar());
        return "car/car-add";
    }

    @PostMapping("/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:car:add')")
    public Result add(SysCar sysCar){

        return carService.add(sysCar);
    }

    @GetMapping("/edit")
    @PreAuthorize("hasAuthority('sys:car:edit')")
    public String editPage(Integer id,Model model){
        SysCar sysCar  = carService.getById(id);
        model.addAttribute("sysCar",sysCar);
        return "car/car-edit";
    }

    @PostMapping("/edit")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:car:edit')")
    public Result edit(SysCar sysCar){

        return carService.edit(sysCar);
    }

    @PostMapping("/del")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:car:del')")
    public Result del(SysCar sysCar){

        return carService.del(sysCar);
    }

    @GetMapping("/search")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:car:query')")
    public Result search(String number,String tel,PageTableRequest pageTableRequest){
        pageTableRequest.countOffset();
        return carService.search(number,tel,pageTableRequest.getOffset(),pageTableRequest.getLimit());
    }
}
