package com.hxm.service;

import com.hxm.util.Result;

/**
 * @date 2020/12/3 17:29
 */
public interface RoleUserService {
    Result getRoleUserByUserId(Integer userId);
}
