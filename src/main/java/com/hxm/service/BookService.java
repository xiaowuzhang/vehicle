package com.hxm.service;

import com.hxm.model.SysBook;
import com.hxm.util.Result;

/**
 * @date 2020/12/4 17:22
 */
public interface BookService {
    Result getList(Integer offset, Integer limit);

    Result add(SysBook sysBook);

    SysBook getById(Integer id);

    Result edit(SysBook sysBook);

    Result del(SysBook sysBook);

    Result search(String number, Integer offset, Integer limit);
}
