package com.hxm.service;

import com.hxm.util.Result;

/**
 * @date 2020/12/3 15:34
 */
public interface PermissionService {
    Result listAllPermission();

    Result listAllPermissionByRoleId(Integer id);

    Result getMenu(Integer userId);
}
