package com.hxm.service;

import com.hxm.dto.RoleDto;
import com.hxm.model.SysRole;
import com.hxm.util.Result;

/**
 * @date 2020/12/3 17:23
 */
public interface RoleService {
    Result getAllRole();

    Result deleteRoleById(Integer id);

    Result search(String rolename);

    Result addRole(RoleDto roleDto);

    Result editRole(RoleDto roleDto);

    SysRole getRole(Integer id);
}

