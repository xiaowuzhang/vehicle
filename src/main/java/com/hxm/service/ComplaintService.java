package com.hxm.service;

import com.hxm.model.SysComplaint;
import com.hxm.util.Result;

/**
 * @date 2020/12/4 1:24
 */
public interface ComplaintService {
    Result getList(Integer offset, Integer limit);

    Result add(SysComplaint sysComplaint);

    Result handle(SysComplaint sysComplaint);
}
