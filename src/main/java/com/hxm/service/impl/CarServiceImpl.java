package com.hxm.service.impl;

import com.hxm.dao.CarDao;
import com.hxm.model.SysCar;
import com.hxm.service.CarService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @date 2020/12/4 2:30
 */
@Service
public class CarServiceImpl implements CarService {
    @Autowired
    CarDao carDao;

    @Override
    public Result getList(Integer offset, Integer limit) {
        return Result.ok(carDao.countAll().intValue(),carDao.getList(offset,limit));
    }

    @Override
    public Result add(SysCar sysCar) {
        Integer add = carDao.add(sysCar);
        if (add > 0){
            return Result.ok();
        }
        return Result.build(500, "操作失败");
    }

    @Override
    public SysCar getById(Integer id) {
        return carDao.getById(id);
    }

    @Override
    public Result edit(SysCar sysCar) {
        Integer update = carDao.update(sysCar);
        if (update > 0){
            return Result.ok();

        }
        return Result.build(500, "操作失败");
    }

    @Override
    public Result del(SysCar sysCar) {
        int delete = carDao.delete(sysCar.getId());
        if (delete > 0){
            return Result.ok();
        }
        return Result.build(500, "操作失败");
    }

    @Override
    public Result search(String number, String tel,Integer offset, Integer limit) {
        return Result.ok(carDao.countBySearch("%" + number + "%", "%" + tel + "%"),carDao.search("%" + number + "%", "%" + tel + "%", offset, limit));
    }
}
