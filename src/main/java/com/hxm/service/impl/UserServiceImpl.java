package com.hxm.service.impl;

import com.hxm.dao.RoleUserDao;
import com.hxm.dao.UserDao;
import com.hxm.model.SysRoleUser;
import com.hxm.model.SysUser;
import com.hxm.service.UserService;

import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleUserDao roleUserDao;


    @Override
    public Result getUserList(Integer offset, Integer limit) {
        return Result.ok(userDao.countAllUser().intValue(),userDao.getUserList(offset,limit));
    }

    @Override
    public Result addUser(SysUser sysUser, Integer roleId,Integer departmentId) {
        SysUser find = userDao.getUser(sysUser.getUsername());
        if(find==null){
            if(roleId != null || !roleId.equals(0)){
                sysUser.setPassword(new BCryptPasswordEncoder().encode(sysUser.getPassword()));
                sysUser.setPhone("");
                sysUser.setTelephone("");
                sysUser.setEmail("");
                userDao.addUser(sysUser);


                SysRoleUser sysRoleUser = new SysRoleUser();
                sysRoleUser.setRoleId(roleId);
                sysRoleUser.setUserId(sysUser.getId());
                roleUserDao.add(sysRoleUser);

                return Result.ok();
            }
        }else{
            return Result.build(500,"用户名存在") ;
        }
        return Result.failure();
    }

    @Override
    public SysUser getUserById(Integer id) {
        return userDao.getUserById(id);
    }

    @Override
    public Result updateUser(SysUser sysUser, Integer roleId,Integer departmentId) {
        SysUser find = userDao.getUser(sysUser.getUsername());
        if(find !=null && !(find.getId().equals(sysUser.getId()))){
            return Result.build(500,"用户名存在");
        }
        if(roleId != null){
            userDao.updateUser(sysUser);


            SysRoleUser sysRoleUser = new SysRoleUser();
            sysRoleUser.setRoleId(roleId);
            sysRoleUser.setUserId(sysUser.getId());
            roleUserDao.update(sysRoleUser);
            return Result.ok();
        }
        return Result.failure();
    }

    @Override
    public Result changeStatus(SysUser sysUser) {
        if(sysUser.getId() != null && sysUser.getStatus()!=null){

            userDao.changeStatus(sysUser);
            return Result.ok();
        }
        return Result.failure();
    }

    @Override
    public Result deleteUser(SysUser sysUser) {
        if(userDao.deleteUser(sysUser)>=1){
            return Result.ok();
        }
        return Result.failure();
    }

    @Override
    public Result getUserByFuzzyKeywords(Integer offset, Integer limit, String nickname,String userId,String phone,String cellphone,String department) {

        List<SysUser> list = userDao.getUserByFuzzyKeywords(offset,limit,"%"+nickname+"%","%"+userId+"%","%"+phone+"%","%"+cellphone+"%","%"+department+"%");

        return Result.ok(userDao.countUserByFuzzyKeywords("%"+nickname+"%","%"+userId+"%","%"+phone+"%","%"+cellphone+"%","%"+department+"%"),list);
    }

    @Override
    public Result deleteAllUser(List<SysUser> list) {
        Integer count = 0;
        for (SysUser sysUser : list) {
            if(userDao.deleteUser(sysUser)>=1){
                count++;
            }
        }
        return Result.ok(count.toString());
    }

    @Override
    public SysUser getUserByUsername(String username) {
        return userDao.getUser(username);
    }


    @Override
    public Result changePassword(SysUser sysUser, String oldpassword) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean flag = encoder.matches(oldpassword,userDao.getUserAndPasswordById(sysUser.getId()).getPassword());
        if(flag){
            sysUser.setPassword(new BCryptPasswordEncoder().encode(sysUser.getPassword()));
            userDao.changePassword(sysUser);
            return Result.ok("true");
        }else{
            return Result.ok("false");
        }
    }

}
