package com.hxm.service.impl;

import com.hxm.dao.FixDao;
import com.hxm.model.SysFix;
import com.hxm.service.FixService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2020/12/3 19:04
 */
@Service
public class FixServiceImpl implements FixService {
    @Autowired
    private FixDao fixDao;

    @Override
    public Result getFixList(Integer offset, Integer limit) {
        return Result.ok(fixDao.countAll().intValue(),fixDao.getList(offset,limit));
    }

    @Override
    public Result add(SysFix sysFix) {
        Integer add = fixDao.add(sysFix);
        if (add > 0){
            return Result.ok();
        }
        return Result.build(500, "操作失败");
    }

    @Override
    public SysFix getById(Integer id) {
        return fixDao.getById(id);
    }

    @Override
    public Result update(SysFix sysFix) {
        Integer update = fixDao.update(sysFix);
        if (update > 0){
            return Result.ok();

        }
        return Result.build(500, "操作失败");
    }

    @Override
    public Result delete(Integer id) {
        System.out.println(id);
        int delete = fixDao.delete(id);
        System.out.println(delete);
        if (delete > 0){
            return Result.ok();
        }
        return Result.build(500, "操作失败");
    }

    @Override
    public Result search(Integer offset, Integer limit, String name) {
        return Result.ok(fixDao.countByFuzzyWord("%"+name+"%").intValue(),fixDao.search(offset,limit,"%"+name+"%"));
    }
}
