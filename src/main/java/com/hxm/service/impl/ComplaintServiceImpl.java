package com.hxm.service.impl;

import com.hxm.dao.ComplaintDao;
import com.hxm.model.SysComplaint;
import com.hxm.service.ComplaintService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @date 2020/12/4 1:29
 */
@Service
public class ComplaintServiceImpl implements ComplaintService {
    @Autowired
    private ComplaintDao complaintDao;

    @Override
    public Result getList(Integer offset, Integer limit) {
        return Result.ok(complaintDao.countAll().intValue(),complaintDao.getList(offset,limit));
    }

    @Override
    public Result add(SysComplaint sysComplaint) {
        Integer add = complaintDao.add(sysComplaint);
        if (add > 0){
            return Result.ok();
        }
        return Result.failure();
    }

    @Override
    public Result handle(SysComplaint sysComplaint) {
        Integer handle = complaintDao.handle(sysComplaint);
        if (handle > 0){
            return Result.ok();
        }
        return Result.failure();
    }
}
