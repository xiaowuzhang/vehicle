package com.hxm.service.impl;


import com.hxm.dao.RoleUserDao;
import com.hxm.model.SysRoleUser;
import com.hxm.service.RoleUserService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleUserServiceImpl implements RoleUserService {

    @Autowired
    RoleUserDao roleUserDao;

    @Override
    public Result getRoleUserByUserId(Integer userId) {
        SysRoleUser sysRoleUser= roleUserDao.getRoleUserByUserId(userId);
        if(sysRoleUser != null){
            return Result.ok(sysRoleUser);
        }
        return Result.ok();
    }
}
