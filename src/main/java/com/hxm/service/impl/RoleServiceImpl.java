package com.hxm.service.impl;

import com.hxm.dao.RoleDao;
import com.hxm.dao.RolePermissionDao;
import com.hxm.dto.RoleDto;
import com.hxm.model.SysRole;
import com.hxm.service.RoleService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleDao roleDao;

    @Autowired
    RolePermissionDao rolePermissionDao;

    @Override
    public Result getAllRole()
    {
        return Result.ok(roleDao.countRole().intValue(),roleDao.getAllRole());
    }

    @Override
    public Result deleteRoleById(Integer id) {
        roleDao.deleteRoleById(id);
        return Result.ok();
    }

    @Override
    public Result search(String rolename) {

        List<SysRole> list = roleDao.search("%"+rolename+"%");

        return Result.ok(list.size(),list);
    }

    @Override
    public Result addRole(RoleDto roleDto) {
        //先加进role表
        roleDao.addRole(roleDto);
        //把list里面的最顶级的节点去掉
        int id = roleDto.getId();
        List<Integer> list = roleDto.getPermissionIds();

        if(list.size() != 0){
            list.remove((Integer)(0));
            for (Integer tmp : list) {
                rolePermissionDao.add(id,tmp);
            }
        }


        return Result.ok();
    }

    @Override
    public Result editRole(RoleDto roleDto) {
        int id = roleDto.getId();
        roleDao.edit(roleDto);
        List<Integer> list = roleDto.getPermissionIds();
        rolePermissionDao.removeAll(id);
        if(list.size() != 0){
            list.remove((Integer)(0));
            for (Integer tmp : list) {
                rolePermissionDao.add(id,tmp);
            }
        }


        return Result.ok();
    }


    @Override
    public SysRole getRole(Integer id) {
        return roleDao.getRoleById(id);
    }
}
