package com.hxm.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.hxm.dao.PermissionDao;
import com.hxm.model.SysPermission;
import com.hxm.service.PermissionService;
import com.hxm.util.Result;
import com.hxm.util.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    PermissionDao permissionDao;

    @Override
    public Result listAllPermission() {

        List datas = permissionDao.listAllPermission();
        JSONArray array = new JSONArray();
        TreeUtils.setPermissionsTree(0,datas,array);
        return Result.ok(array);
    }

    @Override
    public Result listAllPermissionByRoleId(Integer id) {

        List<SysPermission> list = permissionDao.listAllPermissionByRoleId(id);
        return Result.ok(list);
    }

    @Override
    public Result getMenu(Integer userId) {
        List<SysPermission> datas = permissionDao.listPermissionByUserId(userId);
        JSONArray array = new JSONArray();
        TreeUtils.setPermissionsTree(0,datas,array);
        return Result.ok(array);
    }
}
