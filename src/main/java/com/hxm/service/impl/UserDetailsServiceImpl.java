package com.hxm.service.impl;

import com.hxm.dao.PermissionDao;
import com.hxm.model.SysPermission;
import com.hxm.model.SysUser;
import com.hxm.security.LoginUser;
import com.hxm.service.UserService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserService userService;

    @Autowired
    PermissionDao permissionDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = userService.getUserByUsername(username);
        if(sysUser == null){
            throw new AuthenticationCredentialsNotFoundException("用户名不存在");
        }
        List<SysPermission> permissions = permissionDao.listPermissionByUserIdIncludingType2(sysUser.getId());
        List<String> tmp = new ArrayList<>();
        for (SysPermission permission : permissions) {
            String per = permission.getPermission();
            if(!"".equals(per) && per != null){
                tmp.add(per);
            }
        }

        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(sysUser,loginUser);
        loginUser.setPermissions(tmp);

        return loginUser;
    }
}
