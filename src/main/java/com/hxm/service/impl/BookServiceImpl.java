package com.hxm.service.impl;

import com.hxm.dao.BookDao;
import com.hxm.model.SysBook;
import com.hxm.service.BookService;
import com.hxm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @date 2020/12/4 17:22
 */
@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookDao bookDao;

    @Override
    public Result getList(Integer offset, Integer limit) {
        return Result.ok(bookDao.countAll().intValue(),bookDao.getList(offset,limit));
    }

    @Override
    public Result add(SysBook sysBook) {
        Integer add = bookDao.add(sysBook);
        if (add > 0){
            return Result.ok();
        }
        return Result.build(500, "操作失败");
    }

    @Override
    public SysBook getById(Integer id) {
        SysBook sysBook = bookDao.getById(id);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = null;
        try {
            parse = sdf.parse(sysBook.getArriveTime());
        } catch (ParseException e) {
            e.printStackTrace(); // 使用日志
        }
        sysBook.setArriveTime(sdf.format(parse));
        System.out.println(sysBook.getArriveTime());

        return sysBook;
    }

    @Override
    public Result edit(SysBook sysBook) {
        Integer update = bookDao.update(sysBook);
        if (update > 0){
            return Result.ok();

        }
        return Result.build(500, "操作失败");
    }

    @Override
    public Result del(SysBook sysBook) {
        int delete = bookDao.delete(sysBook.getId());
        if (delete > 0){
            return Result.ok();
        }
        return Result.build(500, "操作失败");
    }

    @Override
    public Result search(String number, Integer offset, Integer limit) {
        return Result.ok(bookDao.countBySearch("%" + number + "%"),bookDao.search("%" + number + "%", offset, limit));
    }
}
