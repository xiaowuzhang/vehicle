package com.hxm.service;

import com.hxm.model.SysFix;
import com.hxm.util.Result;

/**
 * @date 2020/12/3 19:04
 */
public interface FixService {
    Result getFixList(Integer offset, Integer limit);

    Result add(SysFix sysFix);

    SysFix getById(Integer id);

    Result update(SysFix sysFix);

    Result delete(Integer id);

    Result search(Integer offset, Integer limit, String name);
}
