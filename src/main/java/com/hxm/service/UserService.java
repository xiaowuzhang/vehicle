package com.hxm.service;

import com.hxm.model.SysUser;
import com.hxm.util.Result;

import java.util.List;

/**
 * @date 2020/12/3 16:00
 */
public interface UserService {
    Result getUserList(Integer offset, Integer limit);

    Result addUser(SysUser sysUser, Integer roleId,Integer departmentId);

    SysUser getUserById(Integer id);

    Result updateUser(SysUser sysUser, Integer roleId,Integer departmentId);

    Result changeStatus(SysUser sysUser);

    Result deleteUser(SysUser sysUser);

    Result getUserByFuzzyKeywords(Integer offset, Integer limit, String nickname,String userId,String phone,String cellphone,String department);

    Result deleteAllUser(List<SysUser> list);

    SysUser getUserByUsername(String username);

    Result changePassword(SysUser sysUser, String oldpassword);


}
