package com.hxm.service;

import com.hxm.model.SysCar;
import com.hxm.util.Result;

/**
 * @date 2020/12/4 2:27
 */
public interface CarService {
    Result getList(Integer offset, Integer limit);

    Result add(SysCar sysCar);

    SysCar getById(Integer id);

    Result edit(SysCar sysCar);

    Result del(SysCar sysCar);

    Result search(String number, String tel,Integer offset, Integer limit);
}
