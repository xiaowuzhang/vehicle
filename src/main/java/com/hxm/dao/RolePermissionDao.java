package com.hxm.dao;

import com.hxm.model.SysRolePermission;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface RolePermissionDao {


    @Insert("insert into sys_role_permission(roleId,permissionId) values(#{roleId},#{permissionId})")
    void add(@Param("roleId") Integer roleId, @Param("permissionId") Integer permissionId);


    @Delete("delete from sys_role_permission where roleId=#{id}")
    void removeAll(@Param("id") int id);

    @Select("select * from sys_role_permission where roleId=#{id}")
    List<SysRolePermission> getPermissionByRoleId(@Param("id") Integer id);
}
