package com.hxm.dao;

import com.hxm.model.SysFix;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @date 2020/12/3 19:11
 */
@Mapper
public interface FixDao {
    @Select("select * from sys_fixing t order by t.id limit #{offset},#{limit}")
    List<SysFix> getList(@Param("offset")Integer offset, @Param("limit")Integer limit);

    @Select("select count(*) from sys_fixing")
    Long countAll();


    @Insert("Insert into sys_fixing(name,num,spec,price) values(#{name},#{num},#{spec},#{price})")
    Integer add(SysFix sysFix);

    @Select("select * from sys_fixing where id=#{id}")
    SysFix getById(Integer id);


    @Update("update sys_fixing set name=#{name},spec=#{spec},price=#{price},num=#{num} where id=#{id}")
    Integer update(SysFix sysFix);

    @Delete("delete from sys_fixing where id=#{id}")
    Integer delete(Integer id);

    @Select("select count(*) from sys_fixing where name like #{name}")
    Long countByFuzzyWord(String name);

    @Select("select * from sys_fixing where name like #{name} limit #{offset},#{limit}")
    List<SysFix> search(Integer offset, Integer limit, String name);
}
