package com.hxm.dao;

import com.hxm.model.SysUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @date 2020/12/3 16:04
 */
@Mapper
public interface UserDao {
    @Select("select * from sys_user t where t.username=#{username}")
    SysUser getUser(String username);

    @Insert("Insert into sys_user(username,password,nickname,phone," +
            "telephone,email,birthday,sex,status,createTime,updateTime)" +
            "values(#{username},#{password},#{nickname},#{phone},#{telephone}," +
            "#{email},now(),#{sex},#{status},now(),now())")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    int addUser(SysUser sysUser);

    @Delete("delete from sys_user where id = #{id}")
    int deleteUser(SysUser sysUser);


    @Select("select t.id,t.username,t.nickname,t.phone,t.telephone," +
            "t.email,t.birthday,t.sex,t.status,t.createTime,t.updateTime " +
            "from sys_user t order by t.id limit #{offset},#{limit}")
    List<SysUser> getUserList(@Param("offset")Integer offset, @Param("limit")Integer limit);

    @Select("select count(*) from sys_user")
    Long countAllUser();


    @Select("select t.id,t.username,t.nickname,t.phone,t.telephone,t.email,t.birthday,t.sex,t.status,t.createTime,t.updateTime from sys_user t where t.id=#{id}")
    SysUser getUserById(Integer id);

    @Update("update sys_user t set t.username=#{username},t.nickname=#{nickname},t.phone=#{phone},t.telephone=#{telephone},t.email=#{email},t.birthday=#{birthday},t.sex=#{sex},t.updateTime=now() where id=#{id}")
    void updateUser(SysUser sysUser);

    @Update("update sys_user t set t.status=#{status} where t.id=#{id}")
    void changeStatus(SysUser sysUser);


    @Select("select * from sys_user t where t.nickname like #{nickname} and t.id like #{userId} and t.phone like #{phone} and t.telephone like #{telephone} order by t.id limit #{offset},#{limit}")
    List<SysUser> getUserByFuzzyKeywords(@Param("offset")Integer offset, @Param("limit")Integer limit, @Param("nickname")String nickname,@Param("userId")String userId,@Param("phone")String phone,@Param("telephone")String telephone,@Param("department")String department);


    @Select("select * from sys_user t where t.nickname like #{nickname} limit #{offset},#{limit}")
    List<SysUser> getUserByNickname(@Param("offset")Integer offset, @Param("limit")Integer limit, @Param("nickname")String nickname);


    @Select("select count(*) from sys_user t where t.nickname like #{nickname} and t.id like #{userId} and t.phone like #{phone} and t.telephone like #{telephone}")
    Integer countUserByFuzzyKeywords(@Param("nickname")String nickname,@Param("userId")String userId,@Param("phone")String phone,@Param("telephone")String telephone,@Param("department")String department);

    @Select("select count(*) from sys_user t where t.nickname like #{nickname}")
    Integer countUserByNickName(@Param("nickname")String nickname);

    @Select("select * from sys_user t where t.id=#{id}")
    SysUser getUserAndPasswordById(@Param("id") Integer id);

    @Update("update sys_user t set t.password=#{password} where t.id=#{id}")
    void changePassword(SysUser sysUser);


    @Select("select t.id,t.username,t.nickname,t.phone,t.telephone," +
            "t.email,t.birthday,t.sex from sys_user t")
    List<SysUser> getUserListWithoutLimit();


}
