package com.hxm.dao;

import com.hxm.model.SysCar;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @date 2020/12/4 2:30
 */
@Mapper
public interface CarDao {
    @Select("select * from sys_car t order by t.id limit #{offset},#{limit}")
    List<SysCar> getList(@Param("offset")Integer offset, @Param("limit")Integer limit);

    @Select("select count(*) from sys_car")
    Long countAll();


    @Insert("Insert into sys_car(number,brand,cname,tel) values(#{number},#{brand},#{cname},#{tel})")
    Integer add(SysCar sysCar);

    @Select("select * from sys_car where id=#{id}")
    SysCar getById(Integer id);


    @Update("update sys_car set number=#{number},brand=#{brand},cname=#{cname},tel=#{tel} where id=#{id}")
    Integer update(SysCar sysCar);

    @Delete("delete from sys_car where id=#{id}")
    Integer delete(Integer id);

    @Select("select * from sys_car t where t.number like #{number} and t.tel like #{tel} order by t.id limit #{offset},#{limit}")
    List<SysCar> search(String number,String tel,@Param("offset")Integer offset, @Param("limit")Integer limit);


    @Select("select count(*) from sys_car t where t.number like #{number} and t.tel like #{tel}")
    Integer countBySearch(String number,String tel);
}
