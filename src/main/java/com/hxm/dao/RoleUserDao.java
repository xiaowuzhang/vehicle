package com.hxm.dao;

import com.hxm.model.SysRoleUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @date 2020/12/3 16:05
 */
@Mapper
public interface RoleUserDao {
    @Insert("insert into sys_role_user(userId,roleId) VALUES(#{userId},#{roleId})")
    void add(SysRoleUser sysRoleUser);

    @Select("select * from sys_role_user t where t.userId = #{userId}")
    SysRoleUser getRoleUserByUserId(Integer userId);


    @Update("update sys_role_user set roleId=#{roleId} where userId=#{userId}")
    void update(SysRoleUser sysRoleUser);
}

