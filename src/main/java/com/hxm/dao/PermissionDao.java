package com.hxm.dao;

import com.hxm.model.SysPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @date 2020/12/3 15:36
 */
@Mapper
public interface PermissionDao {

    @Select("select * from sys_permission")
    List<SysPermission> listAllPermission();

    @Select("select * from sys_permission t where t.id=#{id}")
    SysPermission getPermissionById(@Param("id") Integer id);

    @Select("select p.* from sys_permission p where p.id=ANY(select rp.permissionId from sys_role_permission rp where rp.roleId=#{roleId}) order by p.sort")
    List<SysPermission> listAllPermissionByRoleId(@Param("roleId") Integer roleId);


    @Select("select p.* from sys_permission p where p.id=ANY(select rp.permissionId from sys_role_permission rp where rp.roleId=(select ru.roleId from sys_role_user ru where ru.userId=#{userId})) and p.type=1 order by p.sort")
    List<SysPermission> listPermissionByUserId(@Param("userId") Integer userId);


    @Select("select p.* from sys_permission p where p.id=ANY(select rp.permissionId from sys_role_permission rp where rp.roleId=(select ru.roleId from sys_role_user ru where ru.userId=#{userId})) order by p.sort")
    List<SysPermission> listPermissionByUserIdIncludingType2(Integer id);
}
