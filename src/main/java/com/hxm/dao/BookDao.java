package com.hxm.dao;

import com.hxm.model.SysBook;
import com.hxm.model.SysCar;
import org.apache.ibatis.annotations.*;

import java.util.List;


/**
 * @date 2020/12/4 17:23
 */
@Mapper
public interface BookDao {
    @Select("select * from sys_book t where t.status=0 order by t.id limit #{offset},#{limit}")
    List<SysBook> getList(@Param("offset")Integer offset, @Param("limit")Integer limit);

    @Select("select count(*) from sys_book where status=0")
    Long countAll();


    @Insert("Insert into sys_book(carNumber,clientName,description,arriveTime,status) values(#{carNumber},#{clientName},#{description},#{arriveTime},0)")
    Integer add(SysBook sysBook);

    @Select("select * from sys_book where id=#{id}")
    SysBook getById(Integer id);


    @Update("update sys_book set carNumber=#{carNumber},clientName=#{clientName},description=#{description},arriveTime=#{arriveTime},status=#{status} where id=#{id}")
    Integer update(SysBook sysBook);

    @Delete("delete from sys_book where id=#{id}")
    Integer delete(Integer id);

    @Select("select * from sys_book t where t.carNumber like #{carNumber} order by t.id limit #{offset},#{limit}")
    List<SysBook> search(@Param("carNumber")String number,@Param("offset")Integer offset, @Param("limit")Integer limit);

    @Select("select count(*) from sys_book t where t.carNumber like #{carNumber}")
    Integer countBySearch(String number);
}
