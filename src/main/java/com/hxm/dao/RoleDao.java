package com.hxm.dao;

import com.hxm.dto.RoleDto;
import com.hxm.model.SysRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface RoleDao {

    @Select("select count(*) from sys_role")
    Long countRole();

    @Select("select * from sys_role")
    List<SysRole> getAllRole();

    @Delete("delete from sys_role where id = #{id}")
    void deleteRoleById(@Param("id") Integer id);

    @Select("select * from sys_role t where t.name like #{rolename}")
    List<SysRole> search(@Param("rolename") String rolename);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_role(name,description,createTime,updateTime) values(#{name},#{description},now(),now())")
    int addRole(RoleDto roleDto);

    @Select("Select * from sys_role where id=#{id}")
    SysRole getRoleById(@Param("id") Integer id);

    @Update("update sys_role set name=#{name},description=#{description},updateTime=now() where id=#{id}")
    void edit(RoleDto roleDto);
}
