package com.hxm.dao;

import com.hxm.model.SysComplaint;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @date 2020/12/4 1:29
 */
@Mapper
public interface ComplaintDao {
    @Select("select count(*) from sys_complaint")
    Long countAll();

    @Select("select * from sys_complaint t order by t.id limit #{offset},#{limit}")
    List<SysComplaint> getList(Integer offset, Integer limit);


    @Insert("Insert into sys_complaint(content,createTime,updateTime,status) values(#{content},now(),now(),0)")
    Integer add(SysComplaint sysComplaint);


    @Update("Update sys_complaint set status=1,updateTime=now() where id=#{id}")
    Integer handle(SysComplaint sysComplaint);
}
