package com.hxm.model;

/**
 * @date 2020/12/3 19:06
 */
public class SysFix {
    private Integer id;
    private String name;
    private Integer num;
    private String spec;
    private String price;

    @Override
    public String toString() {
        return "SysFix{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", num=" + num +
                ", spec='" + spec + '\'' +
                ", price='" + price + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
