package com.hxm.model;

public class SysRolePermission {
    private Integer roleId;
    private Integer permissionId;

    @Override
    public String toString() {
        return "SysRolePermission{" +
                "roleId=" + roleId +
                ", permissionId=" + permissionId +
                '}';
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
}
