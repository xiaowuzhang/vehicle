package com.hxm.model;

public class SysRoleUser {
    private Integer userId;
    private Integer roleId;


    @Override
    public String toString() {
        return "SysRoleUser{" +
                "userId=" + userId +
                ", roleId=" + roleId +
                '}';
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
