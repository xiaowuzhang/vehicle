package com.hxm.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

/**
 * @date 2020/12/3 15:18
 */
public class Result implements Serializable {
    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    // 响应业务状态
    private Integer status;

    // 响应消息
    private String msg;

    // 响应中的数据
    private Object data;

    // 响应中的数据数目
    private Integer count;

    public static Result build(Integer status, String msg, Object data) {
        return new Result(status, msg, data);
    }

    public static Result ok(Object data) {
        return new Result(data);
    }

    public static Result ok(Integer count,Object data) {
        return new Result(data,count);
    }

    public static Result  ok() {
        return new Result(null);
    }

    public static Result failure() {
        return new Result(500,"请求错误",null);
    }

    public Result() {

    }

    public static Result build(Integer status, String msg) {
        return new Result(status, msg, null);
    }

    public Result(Integer status, String msg, Object data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public Result(Integer status, String msg, Object data,Integer count) {
        this.status = status;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }

    public Result(Object data) {
        this.status = 200;
        this.msg = "OK";
        this.data = data;
    }

    public Result(Object data,Integer count) {
        this.status = 200;
        this.msg = "OK";
        this.data = data;
        this.count = count;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
