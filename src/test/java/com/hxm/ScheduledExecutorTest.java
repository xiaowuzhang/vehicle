package com.hxm;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorTest {
    private static Map<String, String> localCache = new HashMap<>();
    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);

    static {
        localCache.put("user1", "value1");
        localCache.put("user2", "value2");
        localCache.put("user3", "value3");
    }

    @Test
    public void testScheduledExecutor(){
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // 定时刷新本地缓存

            }
        }, 1, 10, TimeUnit.MILLISECONDS);
    }

}
